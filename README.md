# buran-eventbus

PubSub event bus server for buran

# Docker Run

1. Build

docker build -t buran-eventbus .

2. Run

docker run -d -it -p {SPECIFY_OUTSIDE_PORT}:3333 buran-eventbus