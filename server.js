const EVENT_PORT = 3333

var http = require('http'),
    faye = require('faye');

var server = http.createServer(),
    bayeux = new faye.NodeAdapter({mount: '/'});

bayeux.attach(server);
server.listen(EVENT_PORT, () => console.log(`Event server listening on port ${EVENT_PORT}`));